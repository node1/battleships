/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.yumble.schiffeversenken.game;

import net.yumble.schiffeversenken.game.SchiffeVersenken;

/**
 *
 * @author xFighter911
 */
public interface MapUpdateListener {
 
    public void update(int x, int y, SchiffeVersenken.Field oldValue, SchiffeVersenken.Field newValue);
}
