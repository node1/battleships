/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.yumble.schiffeversenken.game;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author xFighter911
 */
public class SchiffeVersenken {

    private static final int FAILED_PLACEMENT_THRESHOLD = 1000;
    private int numShips2Fields;
    private int numShips3Fields;
    private int numShips4Fields;

    public enum Field {
        WATER, WATER_HIDDEN_SHIP, HIT, WATER_HIT
    };

    int width, height;

    Field[] field;

    private final ObservableList<MapUpdateListener> mapUpdateListener;

    public SchiffeVersenken(int fieldSizeX, int fieldSizeY, int numShips2Fields, int numShips3Fields, int numShips4Fields) {

        this.mapUpdateListener = FXCollections.observableArrayList();

        initialize(fieldSizeX, fieldSizeY, numShips2Fields, numShips3Fields, numShips4Fields);
    }

    public void initialize(int fieldSizeX, int fieldSizeY, int numShips2Fields, int numShips3Fields, int numShips4Fields) {
        this.width = fieldSizeX;
        this.height = fieldSizeY;
        field = new Field[fieldSizeX * fieldSizeY];

        generateShips(2, numShips2Fields);
        generateShips(3, numShips3Fields);
        generateShips(4, numShips4Fields);

        this.numShips2Fields = numShips2Fields;
        this.numShips3Fields = numShips3Fields;
        this.numShips4Fields = numShips4Fields;
    }

    public void restart() {
        clear();
        initialize(width, height, numShips2Fields, numShips3Fields, numShips4Fields);
    }

    private void generateShips(int length, int count) {

        int fails = 0;
        while (count > 0) {
            boolean align90degrees = (Math.random() * 2 >= 1.0d) ? true : false;

            int randX = (int) Math.round(Math.random() * (width - 1));
            int randY = (int) Math.round(Math.random() * (height - 1));

            boolean canPlaceShip = placeShip(randX, randY, align90degrees, length, false /* simulate */);

            if (canPlaceShip) {
                placeShip(randX, randY, align90degrees, length, true /* final placement of the ship */);
                count--;
            } else {
                fails++; // Fehlveruche hochzählen. Wenn das zuviele werden, dann muss abgebrochen werden. Mit vernünftigen Parametern passiert das nicht
            }

            if (fails > FAILED_PLACEMENT_THRESHOLD) {
                // throw some exception 
                return;
            }
        }
    }

    private boolean placeShip(int randX, int randY, boolean align90degrees, int length, boolean simulatePlacement) {
        boolean overlapping = false;
        int stepx, stepy;

        if (!align90degrees) {
            stepx = 1;
            stepy = 0;
        } else {
            stepx = 0;
            stepy = 1;
        }

        int checkx = randX, checky = randY;
        for (int i = 0; i < length; i++) {
            if (simulatePlacement) {
                if (getFieldAt(checkx, checky) != Field.WATER) {
                    return false; // ship can not be placed here as it would overlap another one
                }
            } else {
                setFieldAt(checkx, checky, Field.WATER_HIDDEN_SHIP);
            }

            checkx += stepx;
            checky += stepy;

            if (checkx >= width) {
                checkx = 0;

            }
            if (checky >= height) {
                checky = 0;
            }
        }

        return true;
    }

    public Field getFieldAt(int x, int y) {
        // Überprüfung der Koordinaten hinzufügen
        return field[y * width + x];
    }

    public void setFieldAt(int x, int y, Field value) {
        // Überprüfung der Koordinaten hinzufügen
        Field old = field[y * width + x];
        field[y * width + x] = value;
        fireMapUpdate(x, y, old, value);
    }

    public void clear() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                setFieldAt(x, y, Field.WATER);
            }
        }
    }

    /**
     * @param x
     * @param y
     *
     * @return true wenn das Feld ein Wasserfeld ist, auf das man feuern kann
     */
    public boolean canFireUpon(int x, int y) {
        if (getFieldAt(x, y) == Field.WATER) {
            return true;
        }
        return false;
    }

    /**
     * @return true - wenn alle Schiffe zerstört sind, sonst false
     */
    public boolean hasFoundAll() {
        for (int i = 0; i < width * height; i++) {
            if (field[i] == Field.WATER_HIDDEN_SHIP) {
                return false;
            }
        }

        return true;
    }

    /**
     * Convienence Funktion zum Debuggen Das Spielfeld wird auf der Console
     * ausgegeben
     */
    public void print() {
        for (int y = 0; y < height; y++) {
            System.out.print("|");
            for (int x = 0; x < width; x++) {
                switch (getFieldAt(x, y)) {
                    case WATER:
                        System.out.print(" ");
                        break;
                    case WATER_HIDDEN_SHIP:
                        System.out.print(".");
                        break;
                    case HIT:
                        System.out.print("+");
                        break;
                    default:
                        throw new AssertionError(getFieldAt(x, y).name());

                }
                System.out.print("|");
            }
            for (int x = 0; x < width; x++) {
                System.out.print("+-");
            }
        }
    }

    public ObservableList<MapUpdateListener> getMapUpdateListener() {
        return mapUpdateListener;
    }

    private void fireMapUpdate(int x, int y, Field old, Field nev) {
        for (MapUpdateListener ml : mapUpdateListener) {
            ml.update(x, y, old, nev);
        }
    }

    /**
     * Fire upon a playfield.
     *
     * Be aware : you can fire twice on field you hit already, but no update
     * will be triggered
     *
     * @param x
     * @param y
     * @return true if a ship was hit, otherwise false
     */
    public boolean fire(int x, int y) {
        if (getFieldAt(x, y) == Field.WATER) {
            setFieldAt(x, y, Field.WATER_HIT);
            return false;
        }
        if (getFieldAt(x, y) == Field.WATER_HIT) {
            return false;
        }
        if (getFieldAt(x, y) == Field.WATER_HIDDEN_SHIP) {
            setFieldAt(x, y, Field.HIT);
            return true;
        }

        return false;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
