package net.yumble.schiffeversenken.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import net.yumble.schiffeversenken.game.SchiffeVersenken;
import static net.yumble.schiffeversenken.game.SchiffeVersenken.Field.HIT;
import static net.yumble.schiffeversenken.game.SchiffeVersenken.Field.WATER;
import static net.yumble.schiffeversenken.game.SchiffeVersenken.Field.WATER_HIDDEN_SHIP;
import static net.yumble.schiffeversenken.game.SchiffeVersenken.Field.WATER_HIT;

public class SwingGUI extends JFrame implements ActionListener {

    static ImageIcon ICON_WATER;
    static ImageIcon ICON_HIT;
    static ImageIcon ICON_SCHIFF;

    private final SchiffeVersenken spiel;

    private class ClickListener implements ActionListener {

        protected final int x;
        protected final int y;

        public ClickListener(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
    JButton buttons[][];

    public SwingGUI(SchiffeVersenken spiel) {
        super("Schiffe versenken");

        this.spiel = spiel;

        ICON_WATER = loadIcon("wasser.png");
        ICON_HIT = loadIcon("hit.png");
        ICON_SCHIFF = loadIcon("schiff.png");

        buttons = new JButton[spiel.getWidth()][spiel.getHeight()];

        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridLayout grid = new GridLayout(spiel.getHeight(), spiel.getWidth());
        getContentPane().setLayout(grid);

        for (int j = 0; j < spiel.getHeight(); j++) {
            for (int i = 0; i < spiel.getWidth(); i++) {
                buttons[i][j] = new JButton();
                buttons[i][j].setContentAreaFilled(true);
                buttons[i][j].setBorder(BorderFactory.createEmptyBorder());
                buttons[i][j].setBorderPainted(false);
                buttons[i][j].addActionListener(this);
                buttons[i][j].setMinimumSize(new Dimension(60, 60));

                buttons[i][j].addActionListener(new ClickListener(i, j) {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        spiel.fire(x, y);
                    }
                });

                add(buttons[i][j]);
            }
        }

        spiel.getMapUpdateListener().add((x, y, oldValue, newValue) -> {

            switch (newValue) {
                case WATER:
                    buttons[x][y].setIcon(ICON_WATER);
                    break;
                case WATER_HIDDEN_SHIP:
                    buttons[x][y].setIcon(ICON_WATER);
                    break;
                case HIT:
                    buttons[x][y].setIcon(ICON_HIT);
                    break;
                case WATER_HIT:
                    buttons[x][y].setIcon(ICON_WATER);
                    break;
                default:
                    throw new AssertionError(newValue.name());
            }
        }
        );

        spiel.restart();

        pack();
        setVisible(true);
    }

    public ImageIcon loadIcon(String Name) {
        return new ImageIcon(getClass().getClassLoader().getResource("images/" + Name)); //klasse ImageIcon um Bilder zu laden
    }

    @Override
    public void actionPerformed(ActionEvent e) {

//        if (e.getSource() == field) {
//            System.out.println("pressed");
//            if (spielFeldPanel.isVisible()) {
//                spielFeldPanel.setVisible(false);
//            }
//        }
    }

//    public void ladeButtons() {
//        int x = 0;
//        for (int i = 0; i <= 9; i++) {
//            for (int j = 0; j <= 9; j++) {
//                spielFeldPanel.add(Buttons[i][j] = new JButton(gibBild(IMAGE_WATER)));
//                p2.add(Buttons[i][j] = new JButton(gibBild(IMAGE_WATER)));
//                Buttons[i][j].setBounds(40 + 40 * i, 40 + 40 * j, 40, 40);
//                Buttons[i][j].addActionListener(this);
//                x++;
//
//            }
//        }
//        System.out.println(x);
//
//    }
}
